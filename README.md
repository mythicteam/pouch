# Pouch

Mythic Table's Design System, all in a convenient little Component Pouch.

## Directories

### components

This are the actual Vue components that get used in the application.

### docs

This is the Vue web application that generates the documentation website

### scss

Stores all of our `.scss` files that get generated into the output `.css` files.

### tokens

This stores our central source of truth for design tokens: colors, spacings, etc. It uses [Style Dictionary](https://amzn.github.io/style-dictionary/).

## Build order

You must build the projects in the following order:
1. components
2. tokens
3. scss
4. docs
