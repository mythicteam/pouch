// This file reads the files in componentFolder and generates an index.js file to export them.

const fsp = require('fs').promises

const componentFolder = './src/components/'
const componentIndex = `${componentFolder}index.js`

async function main () {
  // Delete created file if it exists already
  try {
    await fsp.unlink(componentIndex)
  } catch {
    // The only error that would be thrown here is that the file doesn't exist.
    // We don't want this script to fail because of that.
  }
  // This array will be used for the export statement later
  let componentList = []

  // Read the folder containing the components
  let componentFileList = await fsp.readdir(componentFolder).then(list => list.filter(c => c.includes('.vue')))

  // Create the index.js file, starting with this comment
  await fsp.appendFile(componentIndex, `// This file is generated automatically. Do not edit.\n\n`)

  // Go through each file and create its import statement
  for (let cf = 0; cf < componentFileList.length; cf++) {
    // The component name is based on the file name
    let componentName = `Pch${componentFileList[cf].replace('.vue', '')}`
    // Add it to the list of import statements
    await fsp.appendFile(componentIndex, `import ${componentName} from './${componentName.replace('Pch', '')}'\n`)
    // Add this to the componentList for use in export
    componentList.push(componentName)
  }

  // Generate export statement
  await fsp.appendFile(componentIndex, `\nexport { ${componentList.toString().replace(/,/g, ', ')} }\n`)
  console.log('✔️ Components collected successfully')
}

main()
