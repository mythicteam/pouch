const vueDocs = require('vue-docgen-api');
const fsp = require('fs').promises;
const globby = require('globby');
const showdown = require('showdown');
const converter = new showdown.Converter();

const input = 'src/components';
const output = '../docs/PchDocs.js';

(async function () {
  // Delete the file if it exists already
  try {
    await fsp.unlink(output)
  } catch {
    // The only error that would be thrown here is that the file doesn't exist.
    // We don't want this script to fail because of that.
  }
  await fsp.appendFile(output, `/* This file is auto-generated. Do not edit directly */\n\nexport default `, (err) => {
    if (err) throw err
  })

  const componentList = await globby([input])
  const vueFiles = componentList.filter(fileName => fileName.match(/.vue$/g))

  // We want to check for any paired .md files, as well as convert MD to HTML
  const allDocs = async _ => {
    const promises = vueFiles.map(async component => {
      let firstPass = await vueDocs.parse(component);
      let parsed = await (async function () {
        if (!firstPass.docsBlocks) firstPass.docsBlocks = ['']
        if (!firstPass.docsBlocks[0]) {
          const mdFile = await globby([component.replace('vue', 'md')])
          if (mdFile.length) {
            await fsp.readFile(mdFile[0], 'utf-8').then(data => {
              firstPass.docsBlocks.splice(0, 1, data);
            })
          }
        }
        for (let [index, docBlock] of firstPass.docsBlocks.entries()) {
          let converted = converter.makeHtml(docBlock)
          // Take any code examples, and put the actual code right above it
          let snippet = converted.match(/\<pre\>(.*?)\<\/pre>/sg);
          if (snippet) {
            for (let [index, snip] of snippet.entries()) {
              let code = snip.match(/\&.*?\<\/code>/s)[0].replace('</code>', '').replace(/\&lt;/g, '<').replace(/\&gt;/g, '>')
              converted = converted.replace(snip, `${code}\n${snip}`)
            }
          }
          firstPass.docsBlocks.splice(index, 1, converted)
        }
        return firstPass
      })();
      return parsed
    })
    const docsArray = await Promise.all(promises)
    return docsArray
  }

  // Write the components to the output file
  fsp.appendFile(output, JSON.stringify(await allDocs()), (err) => {
    if (err) throw err
  })

})();
