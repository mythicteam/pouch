# Documentation

## Purpose

This Vue website takes in all of the components generated from `../components` and the CSS generated from `../scss` and `../tokens`, and auto-documents them. As of yet, there's no great pipeline for simultaneously running *everything* in a hot-reload method. If this app complains about not being able to find a resource, stop your server, build the part it's complaining about, and start the server again.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
