import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import docs from "../PchDocs.js";
import { PchComponents } from "pouch";
import '../../dist/style/style.css';
import '../../dist/style/vars-dark.css';

import DocPage from "./components/DocPage"

let componentList = Object.keys(PchComponents)
// The list of links for the app's nav
let componentMenuLinks = []

for (let c = 0; c < componentList.length; c++) {
  let pchComponent = PchComponents[componentList[c]]
  console.log(pchComponent)
  Vue.component(pchComponent.name, pchComponent)
  console.log(Vue)
  // Give the file a title
  let pageTitle = pchComponent.name.replace('Pch', '').split(/(?=[A-Z])/g).join(' ')
  // The slug is what shows at the end of the url: localhost:8081/#/components/this-is-the-slug
  let slug = pchComponent.name.replace('Pch', '').split(/(?=[A-Z])/g).join('-').toLowerCase()
  // Add the route, and pass the component into the Sandbox
  router.addRoutes([{
    path: `/components/${slug}`,
    component: {
      template: `<doc-page :docs="docs" />`,
      components: { DocPage },
      data () {
        return {
          component: pchComponent,
          pageTitle: pageTitle,
          docs: docs.find(({ displayName }) => displayName === pchComponent.name)
        }
      }
    }
  }])
  // Add this to the main sidebar nav
  componentMenuLinks.push({ name: pageTitle, slug: slug })
}

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
