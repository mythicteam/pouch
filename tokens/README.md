# Design Tokens

This portion of the design system uses [Style Dictionary](https://amzn.github.io/style-dictionary/#/) to store and generate design tokens. The design tokens are stored in JSON structures, and parsed to be output to various formats to be read. The files currently being output are:

1. `dist/style/vars-dark.css` and `dist/style/vars-light.css`. These are CSS Custom properties that display the light and dark themes of the app.
2. `scss/01-settings/tokens.scss`. This creates a SCSS variables file, where the key is the SCSS variable, and the value is the CSS Custom Property with fallback. `$color-text-base-normal: var(--color-text-base-normal, hsl(0 0 100%));`
3. `docs/resources/tokens.json` is read by /docs to keep an up-to-date list of tokens that developers can use in one-off styles.

Generate tokens with `node index.js` or run `npm run build:tokens` from the root directory.
