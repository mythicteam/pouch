const StyleDictionaryPackage = require('style-dictionary');
// When https://github.com/amzn/style-dictionary/pull/383 is merged, this dependency and the custom transforms can be deleted.
const Color = require('tinycolor2');
function outputStylesheets(theme) {
  return {
    "source": [
      `./tokens/properties/${theme}/**/*.json`,
      "./tokens/properties/common/**/*.json"
    ],
    "platforms": {
      "web": {
        "transforms": ["attribute/cti", "name/cti/kebab", "time/seconds", "content/icon", "size/rem", "color/hsl"],
        "buildPath": `./dist/style/`,
        "files": [{
          "destination": `vars-${theme}.css`,
          "format": "css/variables"
        }]
      },
      "scssDefault": {
        "transforms": ["attribute/cti", "name/cti/kebab", "time/seconds", "content/icon", "size/rem", "color/hsl", "cssCustomPropertyFallback"],
        "buildPath": `./scss/01-settings/`,
        "files": [{
          "destination": `_tokens.scss`,
          "format": "scss/variables"
        }]
      },
      "js": {
        "transforms": ["attribute/cti", "name/cti/kebab", "time/seconds", "content/icon", "size/rem", "color/hsl"],
        "buildPath": `./docs/resources/`,
        "files": [{
          "destination": `tokens.json`,
          "format": "json"
        }],
      }
    }
  };
}

['light', 'dark'].map(function (theme) {
    console.log('\n==============================================');
    console.log(`\nProcessing CSS custom properties for the {theme} theme`);

    const StyleDictionary = StyleDictionaryPackage.extend(outputStylesheets(theme, 'web'));

    StyleDictionary.registerTransform({
      name: 'color/hsl',
      type: 'value',
      matcher: function(prop) {
        return prop.attributes.category === 'color';
      },
      transformer: function(prop) {
        return Color(prop.value).toHslString();
      }
    })

    StyleDictionary.buildPlatform('web');
});

// For the SCSS variables, they're just mapping to a CSS Custom Property with a fallback. `$foo: var(--foo, bar)`. THe dark theme is the fallback. If the CSS Custom Properties are pulled in to the app, that file will be used.

// For the docs, we really just care about the token names. We can use JavaScript to get the values. We're just piggybacking the JSON output onto this function.

['scssDefault', 'js'].map(function (platform) {
  console.log('\n==============================================');
  console.log(`\nProcessing ${platform === 'js' ? 'JSON token docs' : 'SCSS variables'}`);

  const StyleDictionary = StyleDictionaryPackage.extend(outputStylesheets('dark', platform));

  StyleDictionary.registerTransform({
    name: 'color/hsl',
    type: 'value',
    matcher: function(prop) {
      return prop.attributes.category === 'color';
    },
    transformer: function(prop) {
      return Color(prop.value).toHslString();
    }
  })

  StyleDictionary.registerTransform({
    name: 'cssCustomPropertyFallback',
    type: 'value',
    transformer: function(prop) {
      return `var(--${prop.name}, ${prop.value})`;
    }
  })

  StyleDictionary.buildPlatform(platform);


  console.log('\nEnd processing');
})

  console.log('\n==============================================');
  console.log('\nBuild completed!');
